get_voting_blocks <- function(P_marg) {
  
  initials <- as.character(candidate_init$Initials)
  
  labels <- character(NUM_C*(NUM_C - 1)/2)
  i <- 1
  for (u in 1:(NUM_C - 1)) {
    for (v in (u + 1):NUM_C) {
      labels[i] <- sprintf("%s-%s", initials[u], initials[v])
      i <- i + 1
    }
  }
  
  df <- list()
  for (t in 1:T) {
    
    size <- numeric(NUM_C*(NUM_C - 1)/2)
    i <- 1
    for (u in 1:(NUM_C - 1)) {
      for (v in (u + 1):NUM_C) {
        size[i] <- (S[[t]][u,v] + S[[t]][v,u])*P_marg$point[[t]][u,v]
        i <- i + 1
      }
    }
    
    df[[t]] <- data.frame(block = labels, size, stringsAsFactors=FALSE) %>%
      arrange(desc(size), block) %>% 
      top_n(10, size)
      
  }
  
  # Coloring
  set.seed(1990)
  df_agg <- bind_rows(df) %>% group_by(block) %>%
    summarise(max(size)) %>% arrange(desc(`max(size)`)) %>%
    mutate(color = sample(hue_pal()(n())))
  
  tables <- list()
  for (t in 1:T) {
    
    df_t <- df[[t]] 
    for (i in 1:10) {
      df_t[i, 1] <- cell_spec(df_t[i, 1], "latex", 
                              background = df_agg$color[which(df_agg$block == df_t[i, 1])])
    }
    
    tables[[t]] <- df_t %>%
      kable(booktabs = T, escape = F, format = "latex", digits = 1,
            caption = sprintf("$t =$ %d", t)) %>%
      kable_styling(latex_options = c("HOLD_position"),
                    font_size = 9)
  }
  
  
  tables
  
}
