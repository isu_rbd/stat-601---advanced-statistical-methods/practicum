get_prob_cond <- function(Y, P_marg) {
  
  num <- P_cond <- P_cond_lwr <- P_cond_upr <- rep(list(matrix(0, NUM_C, NUM_C)), 
                                                   T - 1)
  # Numerator
  for (b in 1:B) {
    for (j in 2:T) {
      num[[j - 1]] <- num[[j - 1]] + Y[[b]][[j - 1]]*Y[[b]][[j]]
    }
  }
  
  # Denominator
  den <- head(P_marg$point, -1)
  for (j in 1:(T - 1)) {
    den[[j]] <- B*den[[j]]
  }
  
  for (j in 1:(T - 1)) {
    
    for (u in 1:NUM_C) {
      
      for (v in 1:NUM_C) {
        
        if (den[[j]][u, v] != 0) {
          
          p <- P_cond[[j]][u, v] <- num[[j]][u, v]/den[[j]][u, v]
          
          P_cond_lwr[[j]][u, v] <- p + qnorm(0.025)*sqrt( (p*(1 - p)) / B )
          P_cond_upr[[j]][u, v] <- p + qnorm(0.975)*sqrt( (p*(1 - p)) / B )
          
        }
        
      }
      
      P_cond[[j]][den[[j]] == 0] <- NA
      P_cond_lwr[[j]][den[[j]] == 0] <- NA
      P_cond_upr[[j]][den[[j]] == 0] <- NA
      
    }
    
  }
  
  list(lwr = P_cond_lwr, point = P_cond, upr = P_cond_upr)
  
}
