get_table_marg <- function(P, t) {
  
  init <- candidate_init$Initials
  
  lwr <- 100*P$lwr[[t]]; est <- 100*P$point[[t]]; upr <- 100*P$upr[[t]]
  
  CI <- matrix(nrow = NUM_C, ncol = NUM_C)
  
  for (u in 1:NUM_C) {
    for (v in 1:NUM_C) {
      
      if (est[u,v] == 0) {
        est[u, v] <- ""
        CI[u,v] <- ""
        next
      } else {
        est[u,v] <- round(as.numeric(est[u,v]))
        CI[u,v]  <- sprintf("(%.0f, %.0f)", lwr[u,v], upr[u,v])
      }
      
    }
  }
  
  table <- rbind(est, CI) %>% data.frame() %>%
    `colnames<-`(init) %>%
    mutate(" " = rep(init, 2)) %>%  
    arrange(factor(` `, levels = init)) %>%
    select(` `, everything())
  
  ktable <- table %>%
    kable(booktabs = T, escape = F, align = "c", format = "latex", row.names = F,
        caption = sprintf("Marginal probability (in percentage) of edge in survey $t =$ %d", t)) %>%
    row_spec(c(seq(0, 44, by = 4), seq(1, 45, by = 4)), 
             extra_latex_after = "\\rowcolor{gray!10}") %>%
    column_spec(1, border_left = T, border_right = T) %>%
    collapse_rows(columns = 1, latex_hline = "major") %>%
    landscape(margin = "0.25cm")
  
  if (t != 5) {
    ktable %>% kable_styling(latex_options = c("hold_position", "scale_down"))
  } else {
    ktable %>% kable_styling(latex_options = c("hold_position"),
                             font_size = 7)
  }

}
