---
title: "STAT 601 | Practicum (Part IV)"
author: "Ricardo Batista"
date: "4/29/2020"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
source("z. boot/startup.R")
```

# Executive Summary

&nbsp;&nbsp;&nbsp;&nbsp; This final stage of the practicum focuses on analysis of results and model assessment, with emphasis on the former. The analysis primarily deals with two motivating questions posed at the outset: can we identify subpopulations with similar candidate preference and how indicative is a candidate's share of the 2nd choice vote of future performance. Model assessment focuses on assessing whether the actual coverage of the percentile bootstrap confidence intervals is close to the nominal coverage. Other model assessment topics, including cross-validation, are briefly discussed.  


# Analysis 

## Identifying sub popluations with similar candidate preference

&nbsp;&nbsp;&nbsp;&nbsp; Our definition of an edge attempts to indentify sizeable subpopulations of voters which presumably feel relatively indifferent between two given candidates. Tables 1 through 5 on the next page display the top 10 such groups per survey. Group size is given by

$$
\left(\widehat{\mathbb{E}}\left[Y_{u, v}\right]\right)
\left(\widehat{\mathbb{E}}\left[\mathcal{S}_{u,v} + \mathcal{S}_{v,u}\right]\right) 
= \hat p_{u,v}\left(S_{u,v} + S_{v,u}\right).
$$

That is, the subpopulation is the set of voters who voted either $(u,v)$ or $(v,u)$. We use the observed numbers of such voters $S_{u,v}$ and $S_{v,u}$ as an estimate of the actual size of the voting block. Then, we multiply said quantity by $\hat p_{u,v}$, the probabilty that there actually exists signficant commonality -- in the eyes of the foregoing subpopulation of voters -- between the two candidates.  

\pagebreak

```{r echo=FALSE, fig.align="center", fig.height=8, fig.width=5.5, message=FALSE, warning=FALSE}
voting_blocks <- get_voting_blocks(P_marg_np)
```

\vspace{10pt}

\makebox[\linewidth]{$\underline{\text{Ten largest voting blocks per survey}}$}

\makebox[\linewidth]{$\underline{\left(\text{the size of voting block } (u,v) = (v, u) \text{ is defined as} \hspace{5pt} \hat{p}_{u, v} (S_{u,v} + S_{v,u}) \right)}$}
\begin{minipage}[t]{0.2\textwidth}
```{r echo = FALSE, results="asis"}
print(voting_blocks[[1]])
```
\end{minipage}
\begin{minipage}[t]{0.2\textwidth}
```{r echo = FALSE, results="asis"}
print(voting_blocks[[2]])
```
\end{minipage}
\begin{minipage}[t]{0.2\textwidth}
```{r echo = FALSE, results="asis"}
print(voting_blocks[[3]])
```
\end{minipage}
\begin{minipage}[t]{0.2\textwidth}
```{r echo = FALSE, results="asis"}
print(voting_blocks[[4]])
```
\end{minipage}
\begin{minipage}[t]{0.2\textwidth}
```{r echo = FALSE, results="asis"}
print(voting_blocks[[5]])
```
\end{minipage}

\vspace{10pt}

&nbsp;&nbsp;&nbsp;&nbsp; The table also provides insight on who would benefit the most by some of the candidates dropping out of the race. Interestingly, all candidates stood to gain from Elizabeth Warren dropping out, particularly at $t = 1, 2, 3$. The $\text{SB-WE}$ was just one of many potential sources of voters for Warren, but Sanders's lion's share. Note that as time went on, the number of voting blocks with a moderate in it (i.e., Buttigiet, Steyer) grew steadily, at the detriment of more progressive blocks. Lastly, note the model we fit employed $h = 1$, which might be too lax a condition for "inherent appeal." As such, we would be remiss not to fit the model with a few slighly higher values of $h$.  

\vspace{10pt}

## Predictive power of early stage 2nd choice vote share on future performance

&nbsp;&nbsp;&nbsp;&nbsp; Judging by a summary view of first and second choice vote tallies, it would've been tough to predict the many reversals of fortune for candidates throughout the race, particularly Biden's and Sanders's. Our model, however, might hold some predictive power. The stacked area charts in the following page display the observed share of first choice and second choice voters per candidate though time. Compare Warren's share of the first and second choice vote to Biden's. At $t = 1, 2, 3$, she recorded significantly larger numbers than Biden. Now, consider the second set of graphs (p.4). The first line graph displays the estimated expected degree per candidate over time. That is, the $y$ value at time $t$ for candidate $u$ is

$$
\sum_{v \in V(t)} \hat{\mathbb{E}} \left[Y_{u, v}(t)\right] =\sum_{v \in V(t)} \hat p_{u, v}(t).
$$

The reason we care about this metric is because it might serve as a proxy for wide voter appeal. For relatively small surveys like our dataset, thismight prove a more robust metric for long-term performance than the raw first and second choice vote tallies. Notice that although Warren does outperform Biden against this metric, the margin is *significantly* smaller. 

&nbsp;&nbsp;&nbsp;&nbsp; Lastly, the second line graph is a hybrid between the vote tallies and the expected degree. Note it includes the block concept from earlier. The $y$ value at time $t$ for candidate $u$ is

$$
\sum_{v \in V(t)}\left(\widehat{\mathbb{E}}\left[Y_{u, v}\right]\right)
\left(\widehat{\mathbb{E}}\left[\mathcal{S}_{u,v} + \mathcal{S}_{v,u}\right]\right) 
= \sum_{v \in V(t)}\hat p_{u,v}\left(S_{u,v} + S_{v,u}\right).
$$

The graph thus estimates the expected total support from the subgroups defined on the first page.  

\pagebreak

```{r echo=TRUE, fig.align="center", fig.height=8, fig.width=5.5, message=FALSE, warning=FALSE}

graphs <- get_2nd_choice(P_marg_np)
graphs$g_vote_share

```

\pagebreak

```{r echo=TRUE, fig.align="center", fig.height=8, fig.width=7, message=FALSE, warning=FALSE}
plot(graphs$g_degree)
```

\pagebreak

# Model Assessment

## Actual coverage

&nbsp;&nbsp;&nbsp;&nbsp; Recall that to construct the 95% confidence interval in "Part III" we use a nominal coverage level of 95%. This nominal coverage is not necessarily exactly the same as the actual coverage of the percentile bootstrap confidence interval. We may use a simulation study to assess how close the empirical coverage (an approximation for the actual coverage) is to the nominal level. Once such study would be to repeat the bootstrap process outlined in "Part II" (and implemented in "Part III") $M$ times and then assess the degree of overlap across the $M$ Wald confidence intervals. The study for the nonparametric case is as follows:

\vspace{15pt}

----

\vspace{10pt}

For $m \in \{1, ..., M\}$,  

\vspace{10pt}
  
&nbsp;&nbsp;&nbsp; For $t \in \{1, ..., T\}$, 

\vspace{10pt}
  
  1. For $b = 1, ..., B$ repeat the following:
      
      i. Draw a simple random sample $E(t)^{*(m,b)} := \{B_1^{*(m,b)}(t), ..., B_{n_t}^{*(m,b)}(t)\}$ with replacement from $E(t)$, the multiset consisting of the observed responses in survey $t$  
      ii. Compute the set of edges $R^{*(m,b)}(t)$, i.e., the adjacency matrix $Y^{*(m,b)}(t)$  
      
\vspace{10pt}
      
  2. Given the $B$ bootstrap data sets, compute
      
      i. Point estimates $\hat p^{(m)}_{u,v}(t) := \widehat{Pr}(Y_{u, v}(t) = 1)$ and $\hat{c}^{(m)}_{u,v} (t) := \widehat{Pr}\left(Y_{u, v}(t)  = 1| Y_{u, v}(t - 1) = 1\right)$, and  
      ii. Confidence intervals $\hat p^{(m)}_{u,v}(t) \pm z_{0.975} \sqrt{\frac{\hat p^{(m)}_{u,v}(t) \left(1 - \hat p^{(m)}_{u,v}(t)\right)}{B}}$ and $\hat c^{(m)}_{u,v}(t) \pm z_{0.975} \sqrt{\frac{\hat c^{(m)}_{u,v}(t) \left(1 - \hat c^{(m)}_{u,v}(t)\right)}{B}}$.  

\vspace{10pt}

Given the $M$ confidence intervals, for each $\{(u,v) , t\} = \{(v,u) , t\}$, we would 

1. determine the most frequently occuring subset of $[0,1]$ among the $M$ confidence intervals for $p_{u,v}(t) = p_{v,u}(t)$,  
2. pick some value $\hat p'_{v,u}(t)$ within said subset; it will represent our best estimate for $p_{u,v}(t) = p_{v,u}(t)$, and  
3. assess whether the epirical coverage for $\hat p'_{v,u}(t)$, as determined by the $M$ confidence intervals, is close to the nominal coverage 

$$
\hat p'_{v,u}(t) \pm z_{0.975} \sqrt{\frac{\hat p'_{v,u}(t) (1 - \hat p'_{v,u}(t))}{B}}.
$$

\vspace{10pt}

----

\vspace{15pt}

Note the procedure for the parameteric and conditional probabilty would be similar.  

\pagebreak

## Additional model assessment

&nbsp;&nbsp;&nbsp; Instead of estimating $\{p_{v,u}(t)\}$ and $\{c_{v,u}(t)\}$ using all observations (as done in "Part III"), we could produce the estimates via cross-validation:

1. Split the original dataset into k folds
2. For each unique group:  
    
    i. Take the group as the test data set  
    ii. Take the remaining groups as a training data set  
    iii. Fit our model (i.e., produce the $Y$ adjacency matrix) on the training set and evaluate it on the test set  
    iv. Retain the evaluation score (e.g., MSE given by prediction of $Y_{u, v}$ edges)  
    v. Summarize the skill of the model using the sample of model evaluation scores  

If our model performs poorly, perhaps we would reconsider the underlying multinomial generative process in favor of some of the models alluded to earlier which entail some alternative form of aggregating individuals and/or responses. 

&nbsp;&nbsp;&nbsp; Lastly, perhaps we resort to discussing modeling assumptions with the Political Science department and are able to come up with some prior distribution for the edges. We could then use the prior to carry out a posterior predictive check on the estimates produced by our parametric boostrap procedure.  


<!-- # The issue of subgroups -->

<!-- &nbsp;&nbsp;&nbsp;&nbsp;  -->

<!-- - a vector of 5 responses vs a single vdat for each t (exchangeable) -->
<!-- - group voters by the prioritization of issues implied by their (fc, sc) -->
<!-- - group voters by whether they are part of an edge y -->

<!-- # Assess whether bootstrap sample mimics the actual data -->

<!-- &nbsp;&nbsp;&nbsp;&nbsp; Evaluating whether the bootstrap samples agree with the data entails addressing two issues: -->

<!-- 1. How do we assess similarity (between the bootstrap sample and actual data)? -->
<!-- 2. What methods do we use to evaluate said similarity? -->

<!-- Regarding the  -->


\pagebreak

# Code

```{r, code = readLines("z. boot/functions/get_voting_blocks.R"), eval = FALSE}
```

\pagebreak

```{r, code = readLines("z. boot/functions/get_2nd_choice.R"), eval = FALSE}
```

