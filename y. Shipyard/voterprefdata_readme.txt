A few things about the variable naming conventions I used.
 
1)    Civiqs_id is the unique identifier that Civiqs gave us to link responses across waves.
2)    The number after the _ at the end of the variable is the indicator of which wave the variable is from
3)    Any variable that starts “consid” is a binary indicator that the respondent said that he or she was considering the candidate.
4)    Any variable that starts “not” is a binary indicator that the respondents said that he or she does not want the candidate to be the nominee
5)    Variables that are “CandidateName_#” are a binary indicator that the candidate was that person’s first choice
6)    Variables that are “CandidateName2_#” are a binary indicator that the candidate was that person’s second choice
7)    Variables that start “tot” are indicators that the respondent listed the candidate with the initials as a first, second, or considering
8)    Variables that start “net” are coded as a 1 if the candidate is the first, second or being considered and a -1 if the candidate is someone the respondent does not want.
9)    “wtot” is the same as the “tot” variable, expect first choice is a “3”, second choice is a “2”, and considering is a “1”
10) “wnet” is the same as “net” ” variable, expect first choice is a “3”, second choice is a “2”, and considering is a “1”
11) The full list of possible candidates across all the waves are:
1.     Amy Klobuchar
2.     Andrew Yang
3.     Bernie Sanders
4.     Beto O’Rourke
5.     Bill de Blasio
6.     Cory Booker
7.     Elizabeth Warren
8.     Joe Biden
9.     Joe Sestak
10.  John Delaney
11.  Julian Castro
12.  Kamala Harris
13.  Marianne Williamson
14.  Michael Bennet
15.  Pete Buttigieg
16.  Steve Bullock
17.  Tim Ryan
18.  Tom Steyer
19.  Tulsi Gabbard
20.  Wayne Messam
21.  Deval Patrick
22.  Michael Bloomberg
12) For these last 4 variables MB refers to Michael Bennet and MBg refers to Michael Bloomberg.
13) Respondents had two ways of indicating if they didn’t have a preference in the first and second choice questions.  In every wave, we included “unsure.”  After the first wave we added the option “I would not vote in the Democratic Caucus.”  This latter question is captured by the NA variables.  
 
Thanks again for your help with this.  I am sure there are things I left out or are ambiguous.
