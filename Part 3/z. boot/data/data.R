data <- read_delim("z. boot/data/voterpreferencedat.txt", delim = " ")

# Basic
T <- 5; NUM_C <- 23; N <- nrow(data); h <- 1
candidate_init <- read_delim("z. boot/data/candidates.txt", delim = " ",
                             col_names = "Initials") %>%
  data.frame() %>% 
  mutate(type = c("minor", "major", "minor", "major", "major", "minor", 
                  "minor", "minor", "minor", "major", "major", "major", 
                  "minor", "minor", "minor", "minor", "major", "minor", 
                  "major", "major", "minor", "major", "unsure"))
  



# Preprocessing
V <- C <- E <- S <- F_u <- Y <- n <- list()

# Get the T sets of data sets
for (t in 1:T) {
  
  fc <- sprintf("fc%d", t); sc <- sprintf("sc%d", t);
  
  # G(t)
  
  ## E(t)
  E[[t]] <- data %>% select(c("individual", fc, sc)) %>%
    filter(get(fc) != 0, get(sc) != 0,         # Individuals included in survey
           !is.na(get(fc)), !is.na(get(sc)),   # Respondents
           get(fc) != get(sc))                 # fc != sc
  
  ## Basic
  n[[t]] <- nrow(E[[t]])
  V[[t]] <- unique(unlist(select(E[[t]], - individual)))
  C[[t]] <- length(V[[t]])
  
  ## S(t)
  S[[t]] <- get_St(E[[t]])
  
  
  # A(t)
  
  ## F_u(t)
  F_u[[t]] <- sapply(1:NUM_C, function(u) sum(pull(E[[t]], get(fc)) == u))
  
  ## Y(t)
  Y[[t]] <- get_Yt(S[[t]])
  
}

