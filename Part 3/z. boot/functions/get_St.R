get_St <- function(Et) {
  
  Et <- select(Et, - individual)
  Vt <- unique(unlist(Et))
  
  ## S(t)
  St <- matrix(0, NUM_C, NUM_C)
  for (u in Vt) {
    for (v in Vt) {
      St[u, v] <- sum( (Et[,1] == u) * (Et[,2] == v) )
    }
  }
  
  return(St)
  
}
