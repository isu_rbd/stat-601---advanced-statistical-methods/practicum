get_table_marg <- function(P, t) {
  
  lwr <- 100*P$lwr[[t]]; est <- 100*P$point[[t]]; upr <- 100*P$upr[[t]]
  
  table <- data.frame(matrix(nrow = NUM_C, ncol = NUM_C)) %>%
    `colnames<-`(candidate_init$Initials) %>%
    `row.names<-`(sprintf("%s\n", candidate_init$Initials))
  
  
  for (u in 1:NUM_C) {
    for (v in 1:NUM_C) {
      
      if (est[u,v] == 0) {
        table[u,v] <- ""
        next
      }
      
      table[u,v] <- sprintf("%.0f\n(%.0f, %.0f)", 
                            est[u,v], lwr[u,v], upr[u,v])
    }
  }
  
  
  
  table %>%
    mutate_all(linebreak) %>%
    apply(2, function(col) gsub('[l]','[c]', col, fixed = TRUE)) %>%
    kable(booktabs = T, escape = F, align="c", format = "latex",
        caption = sprintf("Marginal probability of edge in survey $t =$ %d (in percentages)", t)) %>%
    kable_styling(latex_options = c("hold_position", "striped"), font_size = 4) %>%
    landscape(margin = "0.5cm")
  # landscape(knitr::kable(head(mtcars), "latex"))

}
