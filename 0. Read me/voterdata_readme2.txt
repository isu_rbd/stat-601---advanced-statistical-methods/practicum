Voter Preference Data

The file voterpreferencedat.txt has 2000 rows and varible names

individual: index
fc1: first choice survey 1
fc2: first choice survey 2
.
.
.
fc6: first choice survey 6
sc1: second choice survey 1
sc2: second choice survey 2
.
.
.
sc6: second choice survey 6
surv1: indicator of inclusion in survey 1 (0=no 1=yes)
surv2: indicator of inclusion in survey 2
.
.
.
surv6: indicator of inclusion in survey 6

All first and second choice variables are integers from 1 to 23 with the following meanings:
Bennet Michael 		1
Biden Joe		2
Booker Cory		3
Bloomberg Michael	4
Buttigieg Pete		5
Bullock Steve		6
Castro Julian		7
deBlasio Bill		8
Delaney John		9
Gabbard Tulsi		10
Harris Kamala		11
Klobuchar Amy		12
Messam Wayne		13
O'Rourke Beto		14
Patrick Deval		15
Ryan Tim		16
Sanders Bernie		17
Sestak Joe 		18
Steyer Tom		19
Warren Elizabeth	20
Williamson Marianne	21
Yang Andrew		22
Unsure			23

Values given as NA are actual missing values,meaning the individual was included in the sample for the survey (and that surv# should be a 1) but did not respond
Values given as 0 for firstchoice and secondchoice mean the individual did not participate in that survey

Note that some individuals (especially in latter surveys) put the same candidate for both first and second choice.  THis is ok, but will need to be taken into account when defining edges for our network.